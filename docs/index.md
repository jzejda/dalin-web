---
# https://vitepress.dev/reference/default-theme-home-page
layout: home

hero:
  name: "DaLin Docs"
  text: "Dokumentace k projektu"
  tagline: Dokumentace k používání projektu. Případně jak pomoci s vývojem.
  actions:
    - theme: brand
      text: Nápověda
      link: /napoveda/index
    - theme: alt
      text: Pomoc s vývojem
      link: /develop/
      

features:
  - title: Jednoduše
    details: Co možná nejvíce zjednodušit agendu správy klubu orientačního běhu maximálně používající ORIS API.
  - title: Postaveno na PHP
    details: Projekt staví na PHP Laravel frameworku. Jedná se celkem standardní PHPko, změny by neměly dělat problém.
  - title: OpenSource
    details: S projektem lze nakládat podle vlastního uvážení. Plánuje se ale pravidelná aktualizace která u forků nemusí fungovat.
---

